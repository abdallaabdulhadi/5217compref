import java.io.IOException;
import java.util.*;

public class CrimeDataAnalysis {
    private static final String CSV1 = "data/crime_data/file1.csv";
    private static final String CSV2 = "data/crime_data/file2.csv";
    private static final String CSV3 = "data/crime_data/file3.csv";

    public static void displayInitialMenu() {
        System.out.println("Crime Data Analysis - Initial Menu:");
        System.out.println("1. Use Optimized Version");
        System.out.println("2. Use Unoptimized Version");
        System.out.println("3. Exit");
        System.out.print("Choose an option: ");
    }

    public static void displayCSVMenu() {
        System.out.println("\nSelect CSV File:");
        System.out.println("1. Load Crime Data from 2023-04");
        System.out.println("2. Load Crime Data from 2023-05");
        System.out.println("3. Load Crime Data from 2023-06");
        System.out.print("Choose an option: ");
    }

    public static void displayAnalysisMenu() {
        System.out.println("\nCrime Data Analysis - Analysis Menu:");
        System.out.println("1. List Distinct Crime Types");
        System.out.println("2. Search Crimes by Type");
        System.out.println("3. Count Investigation Outcomes for the Month");
        System.out.println("4. Find LSOA with Highest Unresolved Crimes");
        System.out.println("5. Find LSOA with Highest Frequency of a Specified Crime Type");
        System.out.println("6. Go Back to Initial Menu");
        System.out.print("Choose an option: ");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<CrimeRecord> data = null;
        boolean exit = false;

        while (!exit) {
            displayInitialMenu();
            int initialChoice = scanner.nextInt();
            scanner.nextLine();  // Consume newline

            switch (initialChoice) {
                case 1:
                case 2:
                    boolean useOptimized = initialChoice == 1;
                    boolean backToInitialMenu = false;

                    while (!backToInitialMenu) {
                        displayCSVMenu();
                        int csvChoice = scanner.nextInt();
                        scanner.nextLine();  // Consume newline

                        String filePath = null;
                        switch (csvChoice) {
                            case 1:
                                filePath = CSV1;
                                break;
                            case 2:
                                filePath = CSV2;
                                break;
                            case 3:
                                filePath = CSV3;
                                break;
                            default:
                                System.out.println("Invalid choice. Please try again.");
                                continue;
                        }

                        try {
                            data = CrimeDataLoader.loadCrimeData(filePath);
                            System.out.println("Loaded " + data.size() + " records from " + filePath);
                        } catch (IOException e) {
                            System.err.println("Error reading data from " + filePath + ": " + e.getMessage());
                            continue;
                        }

                        boolean backToCSVMenu = false;
                        while (!backToCSVMenu) {
                            displayAnalysisMenu();
                            int analysisChoice = scanner.nextInt();
                            scanner.nextLine();  // Consume newline

                            switch (analysisChoice) {
                                case 1:
                                    Set<String> distinctCrimeTypes = useOptimized 
                                        ? CrimeDataLoader.listDistinctCrimeTypes(data)
                                        : UnoptimizedCrimeDataLoader.listDistinctCrimeTypes(data);
                                    System.out.println("Distinct Crime Types: " + distinctCrimeTypes);
                                    break;
                                case 2:
                                    System.out.print("Enter the crime type: ");
                                    String crimeType = scanner.nextLine();
                                    List<CrimeRecord> crimesByType = useOptimized
                                        ? CrimeDataLoader.searchCrimesByType(data, crimeType)
                                        : UnoptimizedCrimeDataLoader.searchCrimesByType(data, crimeType);
                                    crimesByType.forEach(System.out::println);
                                    break;
                                case 3:
                                    if (useOptimized) {
                                        CrimeDataLoader.countInvestigationOutcomes(data);
                                    } else {
                                        UnoptimizedCrimeDataLoader.countInvestigationOutcomes(data);
                                    }
                                    break;
                                case 4:
                                    String highestUnresolvedLSOA = useOptimized 
                                        ? CrimeDataLoader.highestUnresolvedCrimes(data)
                                        : UnoptimizedCrimeDataLoader.highestUnresolvedCrimes(data);
                                    System.out.println("LSOA with the highest unresolved crimes: " + highestUnresolvedLSOA);
                                    break;
                                case 5:
                                    System.out.print("Enter the crime type: ");
                                    crimeType = scanner.nextLine();
                                    String highestCrimeFrequencyLSOA = useOptimized 
                                        ? CrimeDataLoader.highestCrimeFrequencyByType(data, crimeType)
                                        : UnoptimizedCrimeDataLoader.highestCrimeFrequencyByType(data, crimeType);
                                    System.out.println("LSOA with the highest " + crimeType + ": " + highestCrimeFrequencyLSOA);
                                    break;
                                case 6:
                                    backToCSVMenu = true;
                                    break;
                                default:
                                    System.out.println("Invalid choice. Please try again.");
                                    break;
                            }
                        }
                    }
                    break;
                case 3:
                    exit = true;
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        }
        scanner.close();
    }
}

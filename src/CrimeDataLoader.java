import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class CrimeDataLoader {
    public static List<CrimeRecord> loadCrimeData(String filePath) throws IOException {
        List<CrimeRecord> crimeRecords = new ArrayList<>();
        Path path = Paths.get(filePath);
        if (!Files.exists(path)) {
            throw new FileNotFoundException("The file " + filePath + " does not exist.");
        }
        try (BufferedReader br = Files.newBufferedReader(path)) {
            crimeRecords = br.lines()
                             .skip(1) // Skip header
                             .map(CrimeDataLoader::parseCrimeRecord)
                             .filter(Objects::nonNull) // Filter out null records
                             .collect(Collectors.toList());
        }
        return crimeRecords;
    }

    private static CrimeRecord parseCrimeRecord(String line) {
        String[] fields = line.split(",", -1); // Split including trailing empty strings

        // Check if the number of fields is at least 11 (0 to 10)
        if (fields.length < 11) {
            System.err.println("Skipping malformed line: " + line);
            return null; // Return null if the line is malformed
        }

        // Handle optional fields and parse safely
        return new CrimeRecord(fields[0], fields[1], fields[2], fields[3], 
                               parseDoubleSafe(fields[4]), parseDoubleSafe(fields[5]), 
                               fields[6], fields[7], fields[8], fields[9], fields[10]);
    }

    private static double parseDoubleSafe(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return Double.NaN; // Handle missing or invalid numbers
        }
    }

    public static Set<String> listDistinctCrimeTypes(List<CrimeRecord> crimeData) {
        return crimeData.stream()
                        .map(record -> record.crimeType.toLowerCase()) // Convert to lowercase
                        .collect(Collectors.toSet());
    }

    public static List<CrimeRecord> searchCrimesByType(List<CrimeRecord> crimeData, String crimeType) {
        return crimeData.stream()
                        .filter(record -> record.crimeType.equalsIgnoreCase(crimeType)) // Case-insensitive comparison
                        .collect(Collectors.toList());
    }

    public static Map<String, Long> countInvestigationOutcomes(List<CrimeRecord> crimeData) {
        Map<String, Long> outcomesCount = new HashMap<>();
        outcomesCount.put("Under investigation", 0L);
        outcomesCount.put("Investigation complete; no suspect identified", 0L);

        // Using the month of the first record as a reference for display purposes
        String month = crimeData.isEmpty() ? "unknown" : crimeData.get(0).month;

        crimeData.forEach(record -> {
            if (record.lastOutcomeCategory.equalsIgnoreCase("Under investigation")) {
                outcomesCount.put("Under investigation", outcomesCount.get("Under investigation") + 1);
            } else if (record.lastOutcomeCategory.equalsIgnoreCase("Investigation complete; no suspect identified")) {
                outcomesCount.put("Investigation complete; no suspect identified", outcomesCount.get("Investigation complete; no suspect identified") + 1);
            }
        });

        System.out.println("Investigation outcomes for the month: " + month);
        outcomesCount.forEach((outcome, count) -> System.out.println(outcome + ": " + count));
        return outcomesCount;
    }

    public static String highestUnresolvedCrimes(List<CrimeRecord> crimeData) {
        return crimeData.stream()
                        .filter(record -> record.lastOutcomeCategory.equalsIgnoreCase("Investigation complete; no suspect identified"))
                        .collect(Collectors.groupingBy(record -> record.lsoaName, Collectors.counting()))
                        .entrySet()
                        .stream()
                        .max(Map.Entry.comparingByValue())
                        .map(Map.Entry::getKey)
                        .orElse("No unresolved crimes");
    }

    public static String highestCrimeFrequencyByType(List<CrimeRecord> crimeData, String crimeType) {
        return crimeData.stream()
                        .filter(record -> record.crimeType.equalsIgnoreCase(crimeType)) // Case-insensitive comparison
                        .collect(Collectors.groupingBy(record -> record.lsoaName, Collectors.counting()))
                        .entrySet()
                        .stream()
                        .max(Map.Entry.comparingByValue())
                        .map(Map.Entry::getKey)
                        .orElse("No crimes of specified type");
    }
}

public class CrimeRecord {
    String crimeID, month, reportedBy, fallsWithin, location, lsoaCode, lsoaName, crimeType, lastOutcomeCategory;
    double longitude, latitude;

    public CrimeRecord(String crimeID, String month, String reportedBy, String fallsWithin, double longitude,
                       double latitude, String location, String lsoaCode, String lsoaName, String crimeType,
                       String lastOutcomeCategory) {
        this.crimeID = crimeID;
        this.month = month;
        this.reportedBy = reportedBy;
        this.fallsWithin = fallsWithin;
        this.longitude = longitude;
        this.latitude = latitude;
        this.location = location;
        this.lsoaCode = lsoaCode;
        this.lsoaName = lsoaName;
        this.crimeType = crimeType;
        this.lastOutcomeCategory = lastOutcomeCategory;
    }

    @Override
    public String toString() {
        return String.format("CrimeID: %s, Month: %s, Type: %s, Location: %s", crimeID, month, crimeType, location);
    }
}

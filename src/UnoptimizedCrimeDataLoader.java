import java.io.*;
import java.nio.file.*;
import java.util.*;

public class UnoptimizedCrimeDataLoader {
    public static List<CrimeRecord> loadCrimeData(String filePath) throws IOException {
        List<CrimeRecord> crimeRecords = new ArrayList<>();
        Path path = Paths.get(filePath);
        if (!Files.exists(path)) {
            throw new FileNotFoundException("The file " + filePath + " does not exist.");
        }
        try (BufferedReader br = Files.newBufferedReader(path)) {
            String line;
            br.readLine(); // Skip header
            while ((line = br.readLine()) != null) {
                CrimeRecord record = parseCrimeRecord(line);
                if (record != null) {
                    crimeRecords.add(record);
                }
            }
        }
        return crimeRecords;
    }

    private static CrimeRecord parseCrimeRecord(String line) {
        String[] fields = line.split(",", -1); // Split including trailing empty strings

        // Check if the number of fields is at least 11 (0 to 10)
        if (fields.length < 11) {
            System.err.println("Skipping malformed line: " + line);
            return null; // Return null if the line is malformed
        }

        // Handle optional fields and parse safely
        return new CrimeRecord(fields[0], fields[1], fields[2], fields[3], 
                               parseDoubleSafe(fields[4]), parseDoubleSafe(fields[5]), 
                               fields[6], fields[7], fields[8], fields[9], fields[10]);
    }

    private static double parseDoubleSafe(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return Double.NaN; // Handle missing or invalid numbers
        }
    }

    public static Set<String> listDistinctCrimeTypes(List<CrimeRecord> crimeData) {
        Set<String> distinctTypes = new HashSet<>();
        for (CrimeRecord record : crimeData) {
            distinctTypes.add(record.crimeType.toLowerCase());
        }
        return distinctTypes;
    }

    public static List<CrimeRecord> searchCrimesByType(List<CrimeRecord> crimeData, String crimeType) {
        List<CrimeRecord> result = new ArrayList<>();
        for (CrimeRecord record : crimeData) {
            if (record.crimeType.equalsIgnoreCase(crimeType)) {
                result.add(record);
            }
        }
        return result;
    }

    public static Map<String, Long> countInvestigationOutcomes(List<CrimeRecord> crimeData) {
        Map<String, Long> outcomesCount = new HashMap<>();
        outcomesCount.put("Under investigation", 0L);
        outcomesCount.put("Investigation complete; no suspect identified", 0L);

        // Using the month of the first record as a reference for display purposes
        String month = crimeData.isEmpty() ? "unknown" : crimeData.get(0).month;

        for (CrimeRecord record : crimeData) {
            if (record.lastOutcomeCategory.equalsIgnoreCase("Under investigation")) {
                outcomesCount.put("Under investigation", outcomesCount.get("Under investigation") + 1);
            } else if (record.lastOutcomeCategory.equalsIgnoreCase("Investigation complete; no suspect identified")) {
                outcomesCount.put("Investigation complete; no suspect identified", outcomesCount.get("Investigation complete; no suspect identified") + 1);
            }
        }

        System.out.println("Investigation outcomes for the month: " + month);
        outcomesCount.forEach((outcome, count) -> System.out.println(outcome + ": " + count));
        return outcomesCount;
    }

    public static String highestUnresolvedCrimes(List<CrimeRecord> crimeData) {
        Map<String, Long> unresolvedCounts = new HashMap<>();
        for (CrimeRecord record : crimeData) {
            if (record.lastOutcomeCategory.equalsIgnoreCase("Investigation complete; no suspect identified")) {
                unresolvedCounts.put(record.lsoaName, unresolvedCounts.getOrDefault(record.lsoaName, 0L) + 1);
            }
        }

        String maxLSOA = null;
        long maxCount = 0;
        for (Map.Entry<String, Long> entry : unresolvedCounts.entrySet()) {
            if (entry.getValue() > maxCount) {
                maxCount = entry.getValue();
                maxLSOA = entry.getKey();
            }
        }
        return maxLSOA != null ? maxLSOA : "No unresolved crimes";
    }

    public static String highestCrimeFrequencyByType(List<CrimeRecord> crimeData, String crimeType) {
        Map<String, Long> crimeCounts = new HashMap<>();
        for (CrimeRecord record : crimeData) {
            if (record.crimeType.equalsIgnoreCase(crimeType)) {
                crimeCounts.put(record.lsoaName, crimeCounts.getOrDefault(record.lsoaName, 0L) + 1);
            }
        }

        String maxLSOA = null;
        long maxCount = 0;
        for (Map.Entry<String, Long> entry : crimeCounts.entrySet()) {
            if (entry.getValue() > maxCount) {
                maxCount = entry.getValue();
                maxLSOA = entry.getKey();
            }
        }
        return maxLSOA != null ? maxLSOA : "No crimes of specified type";
    }
}
